# Activité du Clibre :

## Culture libriste :

### Énoncer les 4 principes de liberté d'un logiciel libre

[Définition des 4 principes](https://fr.wikipedia.org/wiki/Logiciel_libre#D%C3%A9finition_de_la_Free_Software_Foundation_(FSF))

### Dessiner Richard Stallman

[Richard Stallman](https://fr.wikipedia.org/wiki/Richard_Stallman)

## Ecrire un script Bash :

### 1) Dans le terminal, taper la commande :

```sh
$ vim hello.sh
```

### 2) taper la commande :help pour plus d'informations

N'oubliez pas de sortir du mode `help` pour retourner en mode `edition`

### 3) Taper le script suivant :

```sh
#! /bin/bash

echo "Hello world !"
```

### 4) sauvegarder et quitter le fichier

### 5) rendre le fichier executable 

Avec `chmod`, utiliser `man` ou l'option `--help` pour plus d'informations sur la commande `chmod`.

### 6) executer le fichier :)

## [Bonus] Ecrire un programme en C 

### 1) Dans le terminal, taper la commande :

```sh
$ vim hello.c
```

### 2) Taper le code suivant :

```c
#include <stdio.h>

int main() {
    printf("Hello, World!");
    return 0;
}

```

### 3) Compiler le programme

Avec la commande `gcc`, encore une fois vous pouvez utiliser la commande `man` ou l'option `--help` pour plus d'informations.

### 4) Executer le programme

Exectuez le fichier produit par la compilation.

### 5) Jouer à GameShell

- [GameShell facile](https://gitlab.com/jeansalama/initiation-clibre/blob/master/gameshell-labo02.tar), le jeux explique les commandes à utiliser
- [GameShell difficile](https://gitlab.com/jeansalama/initiation-clibre/blob/master/tp1_inf1070.tar), il faut connaitre un minimum de commandes Unix

## [FUN] Jouer a un jeu Libre ou découvrir des "easter eggs"

Exemple : SuperTux et `apt moo` (`apt moo moo` etc.) ou encore lancer [StarWars](https://itsfoss.com/star-wars-linux/) 
ou faire [rouler un train](https://itsfoss.com/ubuntu-terminal-train/) sur le shell.


